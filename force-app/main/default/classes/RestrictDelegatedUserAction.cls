/**
* Class Name    :   RestrictDelegatedUserAction
* Description   :   This class is used to create demo for how to restrict delegated admin to perform actions.
* Date          :   31 Aug 12
* Author        :   Jitendra Zaa
*/

public class RestrictDelegatedUserAction
{   
   
    public Account acc {get;set;}
    // Boolean veriable to know if user is actual or delegated admin.
    public Boolean isValidUser {get;set;}
    
    public RestrictDelegatedUserAction(ApexPages.standardController sc) {
        
        acc = new Account(Name = 'Rajesh Mewada');
        isValidUser = true;        
    }
    // Constructor of the class
    
    // Method to be called on button submit changes
    public pagereference submitChange()
    {
        ApexPages.Message pageMsg;
        if(isValidUser)
        {
            /**
                Write the logic here to performed by actual user
            **/            
            pageMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Success, Logged in user is actual user');             
        }
        else
        {
            // Create the error message to show to the delegated admin.
            pageMsg = new ApexPages.Message(ApexPages.Severity.Error,'Cannot Perform Task as user is delegated administrator');   
        }
        ApexPages.addMessage(pageMsg); 
        
        return null;
    }
}