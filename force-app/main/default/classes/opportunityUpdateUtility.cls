/**
* Class Name    :   opportunityUpdateUtility
* Description   :   This class will query the oppoerunuty and opportunity product, will show on page and update them.
* Date          :   15 Aug 12
* Author        :   Jitendra Zaa
*/

public class opportunityUpdateUtility
{
    //Opportunity List
    public List<Opportunity> oppList{get;set;}    
    
    // Constructor which will initialise and query the opportunity and opportunity product where current user is owner
    public opportunityUpdateUtility( ApexPages.StandardController controller)
    {        
        oppList = new List<Opportunity>();
        oppList = [Select id, name, closeDate,Account.Name, StageName, probability,Quantity_Q1__c,Quantity_Q2__c,Quantity_Q3__c,Quantity_Q4__c,Total_Amount__c,
         (select id,PricebookEntry.Product2.Name, PricebookEntry.Product2.ProductCode,Competitors__c, unitPrice, Probability__c, Quantity_Q1__c,Quantity_Q2__c,Quantity_Q3__c,Quantity_Q4__c, Total_Values__c from OpportunityLineItems) from opportunity where owner.Id =: UserInfo.getUserId() limit 2];
        if(oppList.isEmpty()){
            ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not own any opportunity');
            ApexPages.addMessage(errorMsg1);     
         }    
    }
    //This method will save the changes in the opportunity and opportunity Product
    Public pagereference saveOpp()
    {
     
     // Creating the save point. If any error occurred we will rollback till this point
     Savepoint sp = Database.setSavepoint();
     try{
         update oppList; // Updaing the opportunity List
         List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
         for(Opportunity opp : oppList)
         for(OpportunityLineItem oli : opp.OpportunityLineItems)
             oliList.add(oli);
         Update oliList; // Updaing all the opportunity products
         
            // Creating the update success message to show on page 
            ApexPages.Message sucessMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Changes are saved successfully !');
            ApexPages.addMessage(sucessMsg);
         return null;
      }Catch(Exception ex)
      {
           // If exception occurred then roll back till savepoint sp
           Database.rollback(sp);
           // Creating error message to show on page
           ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'ERROR: Some error has occurred.');
           ApexPages.addMessage(errorMsg); 
           return null;
      }
    }

}