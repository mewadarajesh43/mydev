global class OwnerNameChangeScheduler implements Schedulable {


     global void execute(SchedulableContext ctx) {
      List<Account> accList = [SELECT Id, Name FROM Account];
      
      for(Account a: accList){
          a.name = 'Updated by scheduler class';
      }
      update accList;
   }   
}