global class LeadAssignmentController
{
    public static void assignLead(List<Lead> newLeads)
    {
        Set<String> locationSet = new Set<String>();
        Set<Id> createdByIdSet = new Set<Id>();
        List<Lead> leadToUpdate = [select id, ownerId, createdById, createdBy.Location__c from Lead where Id IN : newLeads];        
        List<wrapper_UserLeadCount> wrapper_UserLeadCountList = new list<wrapper_UserLeadCount>();

        for(Lead tmpLead : leadToUpdate)
        {
            locationSet.add(tmpLead.CreatedBy.Location__c);
            createdByIdSet.add(tmpLead.createdById);
        }
        
        List<User> userToProcess = [select id, Location__c from user where Id NOT IN: createdByIdSet And Location__c IN: locationSet And At_Work__c = true];
       
        Map<String, List<User>> locationUserMap = new Map<String, List<User>>();       
        Map<String, List<wrapper_UserLeadCount>> locationUseCountMap = new Map<String, List<wrapper_UserLeadCount>>();
        Map<Id,Integer> ownerIdLeadCountMap = new Map<Id,Integer>();
        List<Lead> leadsToProcess = new List<Lead>();
        List<Lead> finalLeadsToUpdate = new List<Lead>();
        if(!userToProcess.isEmpty())
        {
            
            leadsToProcess = [select id, ownerId, createdby.Location__c from Lead where ownerId IN : userToProcess ];
            for(String loc : locationSet)
            {
                
                List<User> tmpUserList = new List<User>();              
                for(User usr : userToProcess)
                {
                    if(loc == usr.Location__c)
                        tmpUserList.add(usr);
                }
                locationUserMap.put(loc,tmpUserList);
            }  
            
                        
            for(Lead ld : leadsToProcess )
            {
            if(ownerIdLeadCountMap.get(ld.OwnerId) == Null)
                ownerIdLeadCountMap.put(ld.OwnerId,1);
            else
                ownerIdLeadCountMap.put(ld.OwnerId,ownerIdLeadCountMap.get(ld.OwnerId)+1);
            }
            
            for(String loc : locationSet)
            {
                List<wrapper_UserLeadCount> tmpWrUsrList = new List<wrapper_UserLeadCount>();
                for(User u : locationUserMap.get(loc) )
                {
                    tmpWrUsrList.add(new wrapper_UserLeadCount(u.id,ownerIdLeadCountMap.get(u.Id)));
                }
                locationUseCountMap.put(loc,tmpWrUsrList);
            }
            
            for(String ownerId : ownerIdLeadCountMap.KeySet())
            {                
                wrapper_UserLeadCountList.add(new wrapper_UserLeadCount(ownerId, ownerIdLeadCountMap.get(ownerId)));
            }
            
            
            
            Map<String, List<Lead>> locationLeadMap = new Map<String, List<Lead>>();
            
            for(String loc : locationSet)
            {
                List<Lead> tmpLeadList = new List<Lead>();
                for(lead tmpLd : leadToUpdate)
                {
                    if(loc == tmpLd.createdBy.Location__c)
                    {
                        tmpLeadList.add(tmpLd);
                    }
                }
                locationLeadMap.put(loc,tmpLeadList);
            
            }
            
            
            for(String loc : locationSet)
            {                
                List<wrapper_UserLeadCount> URLC_List = new List<wrapper_UserLeadCount>();
                URLC_List = locationUseCountMap.get(loc);
                if(!URLC_List.Isempty())
                {
                    Integer option = 1;
                    LeadAssignmentSetting__c ldSetting = LeadAssignmentSetting__c.getValues('Lead Assignment Configuration');
                    if(ldSetting != null)
                        option = (Integer)ldSetting.Distribution_Option__c;
                    if(option == 1)
                    {
                        system.debug('inside else');
                        URLC_List.sort();
                        Integer i = 0;
                        for(Lead ld : locationLeadMap.get(loc))
                        {                            
                            system.debug('owner id else'+URLC_List[i].ownerId);
                            ld.ownerId = URLC_List[i].ownerId;
                            finalLeadsToUpdate.add(ld);
                            if(i== (URLC_List.size()-1))
                                i=0;
                            else
                             i=i+1;
                        }
                    }
                    else
                    {
                        system.debug('inside if');
                        for(Lead ld : locationLeadMap.get(loc))
                        {
                            
                            URLC_List.sort();
                            system.debug('owner Id IF'+URLC_List[0].ownerId);
                            ld.OwnerId = URLC_List[0].ownerId;
                            URLC_List[0].leadCount  = URLC_List[0].leadCount + 1;
                            finalLeadsToUpdate.add(ld);
                        } 
                    
                    }
                }               
            }
            
            update finalLeadsToUpdate;
        }
    
    }
    
    
    
    global class wrapper_UserLeadCount implements Comparable
    {
        Id ownerId;
        Integer leadCount = 0;       
        wrapper_UserLeadCount(Id ownerId, Integer leadCount)
        {
            this.ownerId = ownerId;
            this.leadCount = leadCount;            
        }
        global Integer compareTo(Object compareTo) {
        wrapper_UserLeadCount compareToCount = (wrapper_UserLeadCount )compareTo;
        if (leadCount == compareToCount.leadCount ) return 0;
        if (leadCount > compareToCount.leadCount ) return 1;
        return -1;        
    }
    }

}