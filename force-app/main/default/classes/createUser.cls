public class createUser
{
    public User u {get;set;}
    
    public createUser()
    {
       u = new User();
    }
public pagereference createUserMethod()
{
    try{   
        contact con = new contact(firstName = u.firstName, lastName = u.lastName, AccountId = '0019000000AqkZV');   
        insert con;
        u.ProfileId = [ select id from profile where id = '00e90000000VUKG' ].id;
        u.ContactId = con.id;
        u.TimeZoneSidKey = 'GMT';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.LanguageLocaleKey = 'en_US';
        u.UserPermissionsMobileUser = false;   
        insert u; 
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'User Has been created successfully'));
        return null;
    }
    catch(Exception ex)
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage()));
        return null;
    }
}
}