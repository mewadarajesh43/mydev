public class IndvFinderController {
    
    @AuraEnabled public Contact indv {get;set;}
    
    @AuraEnabled public static sObject searchContact(String fName, String lName)
    {
        system.debug('Raj '+fName);
         system.debug('Raj '+lName);
        List<Contact> conList = [Select id, Name FROM contact where firstName =:fName AND lastName =: lName ];
        return conList[0];
    }
    
}