public class Roles_Group_Democlass
{
Static Set<ID> userIds = new Set<ID>();
Static Set<ID> roleIds = new Set<ID>();
Static Set<ID> roleAndSubordinateIds = new Set<ID>();

public static set<Id> getAllSubGroupsUsers(Set<Id> groupIdsSet){ 
    Set<ID> subGroupIds = new Set<ID>(); 
       
    for(GroupMember gm : [SELECT GroupId, UserOrGroupId FROM GroupMember  WHERE GroupId IN : groupIdsSet])
        if(((String)gm.UserOrGroupId).startsWith('005'))
        {
            userIds.add(gm.UserOrGroupId);
        }
        else
        {            
            subGroupIds.add(gm.UserOrGroupId);            
        }
    if(subGroupIds.size()>0)
        subGroupIds.addAll(getAllSubGroupsUsers(subGroupIds));
    system.debug('subGrp'+subGroupIds);
    return subGroupIds;
 }
 public static Set<Id> getRoleIdsFromGroup(Set<Id> groupIds)
 { 
    Set<Id> currentRoleIdSet = new Set<id>();
    for(Group g : [Select id, relatedId, type from Group where type like '%Role%' AND Id IN : groupIds])
    {
        system.debug('inside for'+g.type);
        if(g.type=='Role')
        {
            system.debug('inside if');
            currentRoleIdSet.add(g.relatedId);
        }
        else
        {
            system.debug('inside else');
            if(g.type=='RoleAndSubordinates')
                roleAndSubordinateIds.add(g.relatedId);
        }
    }
    system.debug('roleidset'+currentRoleIdSet);
    return currentRoleIdSet;
 }
  public static boolean validateUserInGroup(Id userId, String groupName)
  {
    if(getGroupUsers(groupName).contains(userId))
        return true;
    else
        return false;
  }
 
 public static set<ID> getGroupUsers(String groupName) {
        // get requested user's role
       // Id roleId = [select UserRoleId from User where Id = :userId].UserRoleId;
        
        // get all of the roles underneath the user
        //Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
       // system.debug('\n ### allSubRoleIds.size ' + allSubRoleIds.size() + ' => ' + allSubRoleIds);
        
        // get all of the ids for the users in those roles
       // Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :allSubRoleIds]);
          
        //system.debug('\n ### users.size ' + users.size() + ' => ' + users);  
          
        // return the ids as a set so you can do what you want with them
        //return users.keySet();
        Group grp = new Group();
        grp = [Select id from Group where DeveloperName =: groupName];
        roleIds.addAll(getRoleIdsFromGroup(getAllSubGroupsUsers(new Set<Id>{grp.id})));
        if(roleAndSubordinateIds != null && roleAndSubordinateIds.size()>0)
        {
            roleIds.addAll(getAllSubRoleIds(roleAndSubordinateIds));
            for(User usr : [Select id from User where UserRoleId IN : roleIds])
            {
                userIds.add(usr.id);
            }
        }
        
        return userIds;
 
}
 
public static set<ID> getAllSubRoleIds(set<ID> roleId) {
 
        Set<ID> currentRoleIds = new Set<ID>();
 
        // get all of the roles underneath the passed roles
        for(UserRole userRole : [select Id from UserRole where ParentRoleId IN :roleId AND ParentRoleID != null])
            currentRoleIds.add(userRole.Id);
     
        // go fetch some more roles
        if(currentRoleIds.size() > 0)
          currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
     
        return currentRoleIds; 
  }
}