@isTest
public class Roles_Group_DemoclassTest
{
    public static testMethod void Roles_Group_DemoclassTest()
    {
      Test.StartTest();
      group g1 = new Group(DeveloperName='Testgroup1', name='Testgroup1');
      insert g1;
      group g2 = new Group(DeveloperName='Testgroup2', name='Testgroup2');
      insert g2;      
      group g3 = new Group(DeveloperName='Testgroup3', name='Testgroup3');
      insert g3;
     
      userRole role = new UserRole(name='test role');
      insert role;
       userRole role1 = new UserRole(name='test role1',ParentRoleId= role.id);
      insert role1;
      User usr = [Select id, userRoleId from user where id = : UserInfo.getUserId()];
      usr.userroleId = role.id;    
      update usr;
      group g4 = [Select id from Group where relatedId =: role.id limit 1];
      group g5 = [Select id from Group where type='RoleAndSubordinates' limit 1];
      
   
      List<groupMember> gmList = new List<groupMember>();
      groupMember gm1 = new groupMember(groupId=g1.id,UserOrGroupId =g2.id);
      gmList.add(gm1);
      groupMember gm2 = new groupMember(groupId=g2.id,UserOrGroupId =g3.id);
      gmList.add(gm2);
      groupMember gm3 = new groupMember(groupId=g3.id,UserOrGroupId =g5.id);
      gmList.add(gm3);     
       groupMember gm4 = new groupMember(groupId=g2.id,UserOrGroupId =UserInfo.getUserId());
      gmList.add(gm4);
      groupMember gm5 = new groupMember(groupId=g3.id,UserOrGroupId =g4.id);
      gmList.add(gm5);
      
      insert gmList;      
      Roles_Group_Democlass.validateUserInGroup((ID)UserInfo.getUserId(),g1.DeveloperName);
       
      Test.StopTest();
    }

}