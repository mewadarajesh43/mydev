public class myFirstController
{
    public Account acc {get;set;}
    public List<Contact> contactList {get;set;}
    Id accId;
    
    public myFirstController()
    {
        acc = new Account();
        contactList = new List<Contact>();
        accId = ApexPages.currentPage().getParameters().get('id');
        if(accId != Null)
        {
            acc = [Select id, name, AccountNumber, Owner.Name, ownerId from Account where Id =: accId];
        }
    }
    
    public void fetchCOntactRecords()
    {
        contactList = [Select id, LastName, FirstName, Account.Name from Contact where AccountId =: accID];

    }

}