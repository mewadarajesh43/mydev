Public class richTextHelper
{
    
    public string richText {get;set;}
    public richTextHelper()
    {
        Account acc  = [Select id, Text_Rich_Area__c from Account where id = '001900000264uTo' limit 1];
        richText = convertBinary(acc.Text_Rich_Area__c );
        system.debug(richText );
    }
    
    public String convertBinary(String s)
    {
        
        Blob beforeBlob = Blob.valueOf(s);
        string paramvalue = EncodingUtil.base64Encode(beforeBlob );
        //Blob afterblob = EncodingUtil.base64Decode(paramvalue);
        return paramvalue ;
    }
}