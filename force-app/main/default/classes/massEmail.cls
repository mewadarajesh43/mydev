global class massEmail
{
 WebService static Boolean sendEmails(List<Id> ticketIds)
    {   
    
        List<Ticket__c> ticketList = [Select Id,Name,Email_Template_Id__c,Contact__c from Ticket__c where
                                        id IN: ticketIds];
        List<Messaging.SingleEmailMessage> msgList = new List<Messaging.SingleEmailMessage>();
        for(Ticket__c tic: ticketList)
        {                            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTemplateID(tic.Email_Template_Id__c);            
            message.setTargetObjectId(tic.Contact__c);          
            message.setWhatId(tic.Id);
            msgList.add(message);            
        }
       if(msgList.size()>0){
        Messaging.sendEmail(msgList);               
         return true;
        } 
        else
        {
        return false;
        }
    } 
   
}