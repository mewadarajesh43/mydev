/**
*	This class going to be invoked by Ajax Toolkit
*	Used to decrypt the license Key
*/

global class DecryptText {
	/*
	*	In this method we are getting encrypted license already
	*/
	webService static String decryptLicense(String encryptedLicense)
	{
		String retVal = '';
		
		/**
      	*	Get encryption key from Custom Setting
      	*/
      	List<System_Parameter__c> sysParList = [Select parameter_name__c, parameter_value__c FROM System_Parameter__c Where parameter_name__c = 'encryptedKey' LIMIT 1];
	
		System_Parameter__c sysPar = null;
		if(sysParList.size() > 0)
		{
			sysPar = sysParList[0];
		}
		
		Blob base64Key = null;
		
		if(sysPar != null)
		{
			base64Key = Encodingutil.base64Decode(sysPar.parameter_value__c);
		}
	
      	Blob licenseNumDecrypted = Encodingutil.base64Decode(encryptedLicense);
      	
      	Blob decryptedData = Crypto.decryptWithManagedIV('AES256', base64Key, licenseNumDecrypted);
      	
      	retVal = decryptedData.toString();
      	return retVal;
	}
	
	/**
	*	In this method we are getting data from SOQL
	*/
	webService static String decryptLicenseSOQL(Id contactId)
   {
   	 String retVal = '';
      List<Contact> con = [SELECT c.Id, a.Id, a.Name,a.License_Number__c, a.BillingStreet FROM Contact c, c.account a where c.Id = :contactId];
      if(con.size() > 0)
      {
      	/**
      	*	Get encryption key from Custom Setting
      	*/
      	List<System_Parameter__c> sysParList = [Select parameter_name__c, parameter_value__c FROM System_Parameter__c Where parameter_name__c = 'encryptedKey' LIMIT 1];
	
		System_Parameter__c sysPar = null;
		if(sysParList.size() > 0)
		{
			sysPar = sysParList[0];
		}
		
		Blob base64Key = null;
		
		if(sysPar != null)
		{
			base64Key = Encodingutil.base64Decode(sysPar.parameter_value__c);
		}
	
      	Blob licenseNumDecrypted = Encodingutil.base64Decode(con[0].account.License_Number__c);
      	
      	Blob decryptedData = Crypto.decryptWithManagedIV('AES256', base64Key, licenseNumDecrypted);
      	
      	retVal = decryptedData.toString();
      	
      }
      return retVal;
   }

}