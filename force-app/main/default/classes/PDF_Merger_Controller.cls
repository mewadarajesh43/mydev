public class PDF_Merger_Controller
{
    public Opportunity opp {get;set;}
    
 	List<Attachment> attchmentList;
 	
    public String attachmentIds {get;set;}
    
    public PDF_Merger_Controller(ApexPages.StandardController controller) {
    	
    	attachmentIds = '';
        opp = (Opportunity)controller.getRecord();
        
        attchmentList = [Select id from Attachment where ParentId =: opp.id ];
        
        for(Attachment a : attchmentList )
        {
        	if(attachmentIds == '')
        	{
        		attachmentIds = a.id;
        	}else{
        		attachmentIds = attachmentIds+',' + a.id; 
        	}            
        }
           
    }

}