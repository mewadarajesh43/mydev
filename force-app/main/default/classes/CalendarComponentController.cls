public class CalendarComponentController
{
    public Payment__c payment  {get;set;}
    public List<Event> eventList {get;set;}
    public String resultString{get;set;}
    public String selectedDate {get;set;}
    
    public pageReference dispEvents()
    {
        eventList = new List<Event>();
        system.debug('---'+selectedDate);
        
        DateTime startDate = (DateTime)date.parse(selectedDate);
        system.debug('--- created'+startDate );
        eventList = [SELECT GroupEventType,OwnerId,StartDateTime,Subject,WhatId,WhoId FROM Event Where StartDateTime > =: startDate AND StartDateTime<: startDate .addDays(1) AND OwnerId =:UserInfo.getUserId() ];
        if(eventList!= null && eventList.size()==0)
        {
            resultString = 'No event for selected day';
        }
        else
        {
            resultString = 'The following events are assigned for the selected day';
        }
        
        return null;
    }
}