public with sharing class ActivationDateUtil {
    
    
    public Date calculateActivationDate(String businessHoursName, Integer days) 
    {
        //businessHoursName  = 'TestBH';
        //Integer days = 4;
        //if()
        //{
         //}
        
        Date date1 = Date.today();
        Integer i = 0;
        Integer k = 0;
        for(Integer j=0;j<days;j++ )
        {
           if(isHoliDay(date1.addDays(j)))
           {
                 i++;
           } 
           if(!isBusinessDay(date1.addDays(j),businessHoursName ))
           {
                 k++;
           }
           
         } 
         
         System.debug('Holiday count'+i);
         System.debug('Not Business day count'+k);
         System.debug('Date------>'+Date.Today().addDays(days+i+k));

         return Date.Today().addDays(days+i+k);
        
             

    }
    
   private Boolean isBusinessDay(Date inputDate, String businessHoursName) 
   {
    
     List<Boolean> businessDay = new Boolean[7];
     Date knownSunday = date.newInstance(2013, 1, 6);
     
     List<BusinessHours> bhlist = [SELECT SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime FROM BusinessHours WHERE Name = :businessHoursName];         
     
     if(bhlist.isEmpty()){
        return true;
    }
    BusinessHours bh = bhlist[0];
    //BusinessHours bh = [SELECT SundayStartTime, MondayStartTime, TuesdayStartTime, WednesdayStartTime, ThursdayStartTime, FridayStartTime, SaturdayStartTime FROM BusinessHours WHERE Name = 'TestBH'];         
    
    businessDay[0] = (bh.SundayStartTime != null);
    businessDay[1] = (bh.MondayStartTime != null);
    businessDay[2] = (bh.TuesdayStartTime != null);
    businessDay[3] = (bh.WednesdayStartTime != null);
    businessDay[4] = (bh.ThursdayStartTime != null);
    businessDay[5] = (bh.FridayStartTime != null);
    businessDay[6] = (bh.SaturdayStartTime != null);

    Integer i = Math.mod(Math.abs(knownSunday.daysBetween(inputDate)),7);  // 0 sunday, 1 = monday
    return (businessDay[i]);
    }
    
  
    private Boolean isHoliDay(Date currentDate)
    {
            List<Holiday> holidays = [Select StartTimeInMinutes, Name, ActivityDate From Holiday ];
            if(holidays.isEmpty())
                return false;
            for(Holiday hDay : holidays)
            {
               if(currentDate.daysBetween(hDay.ActivityDate) == 0)
               {
                    return true;
               }
            }
         return false;
    }

}