/**
* Class Name    :   opportunityUpdateUtilityTest
* Description   :   Test class for the opportunityUpdateUtility class
* Date          :   15 Aug 12
* Author		: 	Jitendra Zaa
*
*/

@isTest(SeeAllData = true)
Public class opportunityUpdateUtilityTest
{
    public static testMethod void opportunityUpdateUtility_testMethod()
    {
        Opportunity opp = new Opportunity(Name = 'Test Opp');
        opp.StageName = 'New';
        opp.CloseDate = System.Today()+30;
        Insert opp;
        system.AssertNotEquals(opp.Id, Null);
        
        //SELECT Description, Name, IsActive, CreatedById, CreatedDate, IsDeleted, IsStandard, LastModifiedById, LastModifiedDate, Id, SystemModstamp FROM Pricebook2
        
        //Pricebook2 pb1 = new Pricebook2(Name = 'Test Price Book',IsActive = true);
        //insert pb1;
        // querying pricebook
        Pricebook2 pb= [select Id from PriceBook2 where isStandard = true limit 1];
        
        //Inserting product
        Product2 prod = new Product2(Name = 'Test Product', IsActive=true);
        Insert prod;
        system.AssertNotEquals(prod.Id, Null);
        
        //Inserting record in pricebook entry
        PricebookEntry pe = new PricebookEntry(Product2Id= prod.Id, UnitPrice = 1, PriceBook2Id=pb.Id, UseStandardPrice = false, IsActive=true);
        insert pe;
        system.AssertNotEquals(pe.Id, Null);
        
        //Inserting opportunity product
        OpportunityLineItem oli = new OpportunityLineItem(PricebookEntryId = pe.Id, OpportunityId = opp.Id, UnitPrice = pe.UnitPrice, Quantity = 1);
        Insert oli;
        system.AssertNotEquals(oli.Id, Null);
        
         //Creating standard controller
         ApexPages.StandardController controller =  new ApexPages.StandardController(opp);
         // creating object of opportunityUpdateUtility class
         opportunityUpdateUtility oUtility = new opportunityUpdateUtility(controller);
          System.AssertNotEquals(oUtility.OppList, Null);
          
         // calling saveOp method of opportunityUpdateUtility class
         oUtility.OppList[0].Quantity_Q1__c = 5000;         
         oUtility.SaveOpp();
         System.AssertEquals(oUtility.OppList[0].Quantity_Q1__c,5000);
         
         //Making the close date Null to create exception to cover the catch block
         oUtility.OppList[0].closeDate = Null;
         oUtility.SaveOpp();
    }
}