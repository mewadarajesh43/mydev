public class TransientExample{

    DateTime t1;
    transient static DateTime t3;
    transient DateTime t2;
    
    {
        t3 = System.now()+1000;
    }
    public String getT1() {
        if (t1 == null) t1 = System.now();
        return '' + t1;
    }
     public String getT3() {
        
        return '' + t3;
    }

    public String getT2() {
        if (t2 == null) t2 = System.now();
        return '' + t2;
    }
}