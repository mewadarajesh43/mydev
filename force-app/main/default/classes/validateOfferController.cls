public class validateOfferController
{
     String OfferId;
     Offer__c ofr;
     public String payId {get;set;}
     public list<Validation_Error__c> errorList{get;set;}
     public List<Payment_Rates__c> matchingPayrateList{get;set;}
    public validateOfferController(ApexPages.StandardController controller) {
        OfferId = ApexPages.CurrentPage().getParameters().get('id');
        ofr = new Offer__c();
        if(OfferId != NULL && OfferId != '')
            ofr = [Select id, Candidate__c,Candidate__r.Age__c, Payment_Rate__c, Grade_Id__c, Job_Code__c, Pay_Zone__c, Post_Code__c, Service_Code__c, Status__c from Offer__c where Id =: OfferId];       
        matchingPayrateList = new validateOffer(ofr).getMatchingPayrates(ofr);
        errorList = new list<Validation_Error__c>();
        errorList = [Select id, Err_Msg__c, Is_Corrected__c, Is_Overriden__c, Override_Notes__c from Validation_Error__c where Is_Corrected__c = false AND Is_Overridebale__c = true AND Is_Overriden__c = false AND Offer__c =: ofr.id];
    }

   public pageReference back()
   {
       return new pagereference('/'+OfferId);
   }
   public pagereference updateOffer()
   {
      
       ofr.isResolved__c = true;
       ofr.Payment_Rate__c = matchingPayrateList[0].id;
       
       try
       {
          update ofr;
           list<Validation_Error__c> errorListTmp = new list<Validation_Error__c>();
           errorListTmp = [Select id, Err_Msg__c, Is_Corrected__c from Validation_Error__c where Err_Msg__c like: '%Multiple%' AND Offer__c =: ofr.id AND Is_Corrected__c = false];
           for(Validation_Error__c err: errorListTmp)
            {
                err.Is_Corrected__c = true;
            }
           upsert errorListTmp; 
       }
       catch(Exception ex)
       {return new pagereference('/'+OfferId);}
       return new pagereference('/'+OfferId);
   }
   
   public pagereference updateErros()
   {
       list<Validation_Error__c> errorListTmp = new list<Validation_Error__c>();
       for(Validation_Error__c err: errorList)
       {
           if(err.Override_Notes__c != NULL && err.Override_Notes__c != '' && err.Is_Overriden__c == True)
           {
               errorListTmp.add(err);
           }
       }
       
       try
       {
       upsert errorListTmp;
       }
        catch(Exception ex)
       {return new pagereference('/'+OfferId);}
       return new pagereference('/'+OfferId);
   }
    
}