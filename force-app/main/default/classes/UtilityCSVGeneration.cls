public class UtilityCSVGeneration
{
    
                  
  public final static Map<String, csvSettingWrapper> csvSettings   = new Map<String, csvSettingWrapper>{
     
        'ASN Update'  => new csvSettingWrapper(
                        'Contact',
                        'Account',
                        'Name,Id,Account Name, Account Id', 
                        'Name,Id,Account.Name,Account.id'
                        )
          ,
        'ASN Update Part'  => new csvSettingWrapper(
                        'ASN_SubProduct_Details__c',
                        'ASN_Product_Details__r.ASN_Container_Details__r.ASN_Details__r.Consignment__c',
                        'Product Name,Product Code,ASNMainReference,Identifier Type,Identifier, Manufacturer Country,Manufacturer Name,Pallet Identifier,Carton Identifier', 
                        'ASN_Product_Details__r.ProductName__c,ASN_Product_Details__r.ProductId__c,ASN_Product_Details__r.ASN_Container_Details__r.ASN_Details__r.ASNMainReference__c,IdentifierType__c,Identifier__c,ASN_Product_Details__r.CountryOfManufacture__c,ASN_Product_Details__r.ManufacturerName__c,ASN_Product_Details__r.ASN_Container_Details__r.ASN_Container_Details__r.ContainerId__c,ASN_Product_Details__r.ASN_Container_Details__r.ContainerId__c'
                        )
                        };
    
    
    public static List<Attachment> getAttachmentList(List<String> objectsInCSV, List<Id> parentsIds, String fileName)
    {
        Map<String,Blob> parentIdCSVBlobMap = new Map<String,Blob>();       
        parentIdCSVBlobMap = createCSVContent(objectsInCSV,parentsIds);
        List<Attachment> attachmentList = new List<Attachment>();
        for(String parentId : parentIdCSVBlobMap.keySet())
        {           
            attachmentList.add(new attachment(parentId = parentId, Name = fileName + System.Now()+'.csv',Body = parentIdCSVBlobMap.get(parentId))); 
        }
        insert attachmentList;
        return attachmentList;
    }
  
    public static Map<String,Blob> createCSVContent(List<String> objectsInCSV, List<String> parentsIds)
    {
        Map<String,blob> parentIdCSVSBlobMap = new  Map<String,Blob>();
        String queryString = '';
        String CSVSTring;
        csvSettingWrapper csvSetting; 
        List<String> fieldList = new List<String>();
        Map<String,List<sObject>> parentIdChildsMap;
        Map<String, Map<csvSettingWrapper,List<sObject>>> csvSettingParentIdChildsMap = new Map<String, Map<csvSettingWrapper,List<sObject>>> ();
        for(String obj : objectsInCSV)
        {       
            parentIdChildsMap = new Map<String,List<sObject>>();
            csvSetting = csvSettings.get(obj);          
            queryString = 'Select id, '+ (csvSetting.fieldAPINames.contains('::')? csvSetting.fieldAPINames.replace('::',','): csvSetting.fieldAPINames) + ', '+csvSetting.relationFieldAPIName +' FROM ' + csvSetting.queryObjectAPINaAme + ' WHERE ' + csvSetting.relationFieldAPIName + ' IN : parentsIds';
            for(sObject sObj : Database.query(queryString))
            {
                List<sObject> childLsit = new List<sObject>();
                if(parentIdChildsMap.get(getParentFieldValue(sobj,csvSetting.relationFieldAPIName)) == null)
                {
                    parentIdChildsMap.put(getParentFieldValue(sobj,csvSetting.relationFieldAPIName), new list<sObject>{sObj});
                }                   
                else
                {
                    childLsit = parentIdChildsMap.get(getParentFieldValue(sobj,csvSetting.relationFieldAPIName));
                    childLsit.add(sObj);
                    parentIdChildsMap.put(getParentFieldValue(sobj,csvSetting.relationFieldAPIName),childLsit);
                }
            }
            for(String parentId : parentIdChildsMap.keyset())
            {
                if(csvSettingParentIdChildsMap.get(parentId) == null)
                {
                    csvSettingParentIdChildsMap.put(parentId,new Map<csvSettingWrapper,List<sObject>>{csvSetting => parentIdChildsMap.get(parentId)});
                }
                else
                {
                    //csvSettingParentIdChildsMap.get(parentId).put(new Map<csvSettingWrapper,List<sObject>>{csvSetting => parentIdChildsMap.get(parentId)});
                    Map<csvSettingWrapper,List<sObject>> tempMap = new Map<csvSettingWrapper,List<sObject>>();
                    tempMap   = csvSettingParentIdChildsMap.get(parentId);
                    tempMap.put(csvSetting, parentIdChildsMap.get(parentId));
                    csvSettingParentIdChildsMap.put(parentId,tempMap);
                }
            }
            
        }
        String perObjectCSVString;
        system.debug('@@@ csvSettingParentIdChildsMap'+csvSettingParentIdChildsMap);
        for(String parentId : csvSettingParentIdChildsMap.keySet())
        {
            CSVSTring ='';
            system.debug('@@@ Settings'+csvSettingParentIdChildsMap.get(parentId).keySet());
            system.debug('@@@ Settings size'+csvSettingParentIdChildsMap.get(parentId).keySet().size());
            for(csvSettingWrapper csvSettingTemp : csvSettingParentIdChildsMap.get(parentId).keySet())
            {
                //try{
                perObjectCSVString = '';
                fieldList = csvSettingTemp.fieldAPINames.split(',');
                perObjectCSVString = csvSettingTemp.headers + '\n';
                system.debug('@@@ csvSettingTemp.headers'+csvSettingTemp.headers);
                system.debug('@@@ csvSettingTemp.fields'+csvSettingTemp.fieldAPINames);
                system.debug('@@@ csvSettingParentIdChildsMap'+csvSettingParentIdChildsMap);
                for(sObject sObj : csvSettingParentIdChildsMap.get(parentId).get(csvSettingTemp))
                {
                    for(String field : fieldList)
                    {
                        if(field.contains('::'))
                        {
                            for(String compositField : field.split('::'))
                            {
                                system.debug('@@@ compositField'+compositField);
                                perObjectCSVString = perObjectCSVString + getParentFieldValue(sobj,compositField)+ ' ';
                            }
                        }
                        else
                        {
                            system.debug('@@@ field'+field);
                            perObjectCSVString = perObjectCSVString + getParentFieldValue(sobj,field) + ',';
                        }                       
                    }             
                    perObjectCSVString = perObjectCSVString + '\n'; 
                } 
                perObjectCSVString = perObjectCSVString + '\n\n\n\n\n';
                CSVSTring = CSVSTring + perObjectCSVString;
              //  }
                //catch(Exception ex)
                //{}
            }           
            parentIdCSVSBlobMap.put(parentId,Blob.valueOf(CSVSTring));
                
        }     
        return parentIdCSVSBlobMap;
    }
   public static String getParentFieldValue(sObject sobj, String fieldPath)
   {
       Sobject currentSObject = sObj;       
      system.debug('@@ getting obj'+sObj);
       system.debug('@@ getting field'+fieldPath);
       while (fieldPath.contains('.'))
        {
            List<String> pathPartList   = fieldPath.split ('[.]', 2);
        
                         currentSObject = (SObject) currentSObject.getSobject(pathPartList[0]);
                         if (currentSObject == null) { return null; }
                         fieldPath      = pathPartList[1];
        }       
        return (String.valueOf(currentSObject.get(fieldPath)) == null ? '':String.valueOf(currentSObject.get(fieldPath)));
   }
   
    class csvSettingWrapper
    {      
        public String queryObjectAPINaAme;
        public String relationFieldAPIName;
        public String headers;
        public String fieldAPINames;
        
        public csvSettingWrapper(String queryObjectAPINaAme, String relationFieldAPIName, String headers,String fieldAPINames)
        {
            this.queryObjectAPINaAme = queryObjectAPINaAme;
            this.relationFieldAPIName = relationFieldAPIName;
            this.headers = headers;
            this.fieldAPINames = fieldAPINames;
        }
        
    }
    
}