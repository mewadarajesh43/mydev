/**
 * Commenting for commit
 * 
 * 
 */

public class AccountMatchingController{

    public Account acc{ get; set; }
    public List<Account> matchingAccountList {get; set;}
    
    public AccountMatchingController(ApexPages.StandardController controller) {
       // this.merch = getMerchandise();
       matchingAccountList = new List<Account>();
       acc =  new Account();
    }

    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Account.FieldSets.MatchingAccountFieldSet.getFields();
    }

    public void getMatchingAccount() {
        
        String query = 'SELECT ';
        String whereClouse ='where ';
        Integer rowCount = 0;
        for(Schema.FieldSetMember f : this.getFields()) {
            rowCount++;
            query += f.getFieldPath() + ', ';
            if(this.getFields().size() == rowCount)
            {
                if(acc.get(f.getFieldPath()) != Null)
                    whereClouse += f.getFieldPath() +' = ' + '\'' +acc.get(f.getFieldPath())+ '\'';
                else
                    whereClouse += f.getFieldPath() +' = ' + acc.get(f.getFieldPath());
            }
            else
            {
            if(acc.get(f.getFieldPath()) != Null)
            whereClouse += f.getFieldPath() +' = ' +'\''+ acc.get(f.getFieldPath())+'\'' + ' AND ';
            else
            {
                whereClouse += f.getFieldPath() +' = ' + acc.get(f.getFieldPath())+ ' AND ';
            }
            }
        }
        query += 'Id FROM Account ';
        query += whereClouse;
        system.debug('qqq'+query);
        matchingAccountList = Database.query(query);
        //return matchingAccountList;
    }
    public pageReference createNewAccount()
    {
        String pageReference = '001/e?';
        for(Schema.FieldSetMember f : this.getFields()) {
        if(f.getFieldPath() == 'Name'){
            pageReference += '&acc2' + '=' + acc.get(f.getFieldPath());
        
        }
        else
        {
        if(f.getFieldPath() == 'phone')
             pageReference += '&acc10'+ '=' + acc.get(f.getFieldPath());
        else
            pageReference += '&acc17'+f.getFieldPath()+'='+acc.get(f.getFieldPath());      
        
        }
        }
         system.debug('---'+pageReference);
         return new pagereference(pageReference);
        
    }
    
    public static void sendQuoteEmail(List<Quote> newQuoteList, Map<id, Quote> oldQuoteMap, Boolean IsInsert)
{
    List<Quote> quoteListToProcess = new List<Quote>();
    if(isInsert)
    {
        quoteListToProcess.addAll(newQuoteList);
    }
    else
    {
        Quote oldQuote = new Quote();
        for(Quote q : newQuoteList)
        {
            oldQuote = oldQuoteMap.get(q.id);
            if(q.status != oldQuote.status && q.status == 'Approved')
            {
                quoteListToProcess.add(q);
            }
        }
    }
    if(quoteListToProcess.size()>0)
    {       
        Map<id,List<String>> quoteIdEmailIdListMap = new Map<id,List<String>>();
        quoteIdEmailIdListMap = prepareMapOfQuoteIdEmailList(quoteListToProcess);
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        emailList = preppareEmailToSend(quoteListToProcess, quoteIdEmailIdListMap);
        Messaging.SendEmailResult [] r = 
        Messaging.sendEmail(emailList);
    }
}

public static Map<id,List<String>> prepareMapOfQuoteIdEmailList(List<Quote> quoteList)
{
   return null; 
}

public static List<Messaging.SingleEmailMessage> preppareEmailToSend(List<Quote> quoteList, map<id,List<String>> quoteIdEmailListMap)
{
    List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>(); 
    for(Quote q: quoteList)
    {
        if(quoteIdEmailListMap.get(q.id) != Null && quoteIdEmailListMap.get(q.id).size()>0)
        {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            PageReference p =  Page.QuotePDF;//Replace attachmentPDF with the page u have rendered as PDF
            p.getParameters().put('id',(String)q.Id); 
            p.setRedirect(true);

            // Take the Page content
            Blob b = p.getContent();

            // Create the email attachment
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(q.QuoteNumber+'_Attachment.xls');
            efa.setBody(b);

            // Sets the paramaters of the email 
            email.setSubject( 'Quote attachment' );     
            email.setToAddresses( quoteIdEmailListMap.get(q.Id));
            email.setPlainTextBody( 'Please find the quote document attached' );
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            emailList.add(email);
        }        
    }
    return emailList;
}
 }