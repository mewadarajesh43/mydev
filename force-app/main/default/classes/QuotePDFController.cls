public class QuotePDFController
{
    
    public Quote qt {get;set;}
    public List<QuoteLineItem> qlList {get;set;}
    public String quoteId;
    public String getQuoteId(){ return quoteId; }
    public void setQuoteId(String s){
        quoteId = s;
        QuotePDFController();
    }
    
    public void QuotePDFController()
    {
        //quoteId= apexpages.currentpage().getparameters().get('id');
        qt = new Quote();
        qlList = new List<QuoteLineItem>();
        if(quoteId != Null){
            qt = [Select id,CreatedDate,ExpirationDate,QuoteNumber,CreatedBy.Name,CreatedBy.Phone,CreatedBy.Email,CreatedBy.Fax,Contact.Name,Contact.Phone,Contact.Email,Contact.Fax,
            BillingName,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostalCode,
            ShippingName,ShippingStreet,ShippingCity,ShippingState,ShippingCountry,ShippingPostalCode,
            QuoteToName,QuoteToStreet,QuoteToCity,QuoteToState,QuoteToCountry,QuoteToPostalCode,
            AdditionalName,AdditionalStreet,AdditionalCity,AdditionalState,AdditionalCountry,AdditionalPostalCode, 
            Subtotal,Discount,TotalPrice,Tax,ShippingHandling,GrandTotal from Quote where id =: quoteId];
           qlList = [Select id,PriceBookEntry.Product2.Name,ServiceDate,ListPrice,UnitPrice,Quantity,Discount,TotalPrice from QuoteLineItem where QuoteId =: quoteId];
           }        
    }
}