global class DemoBatchClass implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        String query = 'Select id from Account Limit 1';
        return database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<SObject> scope)
    {
        List<Account> accList = (List<Account>)scope;
        List<Contact> contactList = new list<Contact>();
        for(Account acc: accList)
        {
            system.debug(acc.id);
            contactList.add(new Contact(LastName='CreatedFromDemoBatch', accountId = acc.id));
            
        }
        Insert contactList;
        
    }
    global void finish(Database.BatchableContext bc)
    {
        //Write the finish code here
        
    }

}