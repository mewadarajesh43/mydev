Public class overRideAccountCreation
{

    String recordTypeid;
    public overRideAccountCreation(ApexPages.StandardController controller) {
         recordTypeid = ApexPages.CurrentPage().getParameters().get('RecordType');
    }

    public pagereference checkReDirect()
    {
        recordTypeid = ApexPages.CurrentPage().getParameters().get('RecordType');
        String CurrentUserProfileId = UserInfo.getProfileId();
        Profile pr = [Select id, name from Profile where Id =: CurrentUserProfileId limit 1];
        if(pr.Name == 'Standard User')
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Insufficient access to create Account'));
            return null;
        }
        else
        {
         return new pageReference('/001/e?RecordType='+recordTypeid +'&nooverride=1');
        }
    }

}