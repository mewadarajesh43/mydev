public class CreateFileController
{
    
    public fileContainer filecont{get;set;}
    public CreateFileController()
    {
        filecont = new fileContainer();
    }
    
    class fileContainer
    {
        public File__c file{get;set;}
        public List<Sheet__c> sheets{get;set;}
        public List<Row__c> rows{get;set;}
        public fileContainer()
        {
            file = new file__c();
            sheets = new List<Sheet__c>();
            rows = new List<Row__c>();
            sheets.add(new sheet__c(Column_1__c='Header 1', Column_2__c='Header 2'));
            for(Integer i=0;i<20;i++)
            {
               rows.add(new row__c()); 
            }
            for(Integer i=0;i<1;i++)
            {
               sheets.add(new sheet__c(Name = 'Sheet'+i)); 
            }
            
            
        }
    }

}