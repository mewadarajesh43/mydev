public class validateOffer
{
    Map<String, Validation_Error__c> oldErrorMap = new Map<String,Validation_Error__c>();
    Set<String> faledValidationMsg;
    List<Validation_Error__c> errorToUpsert = new List<Validation_Error__c>();
    String ManualInterventionResult;
    public validateOffer(Offer__c offer)
    {
        for(Validation_Error__c err : [Select id, Err_Msg__c, Is_Corrected__c, Is_Overriden__c, Offer__c from Validation_Error__c where Offer__c =: Offer.Id])
        {
            oldErrorMap.put(err.Err_Msg__c, err);
        }
        faledValidationMsg = new Set<String>();
    }
    
    public void validateOffer(Offer__c tmpOffer)
    {
        offer__c offer = [Select id, Candidate__c,Candidate__r.Age__c,isResolved__c, Payment_Rate__c, Grade_Id__c, Job_Code__c, Pay_Zone__c, Post_Code__c, Service_Code__c, Status__c from Offer__c where id =: tmpOffer.id];
        validateAge(offer);
        validateGradeId(offer);
        validatePostId(offer);
        validateServiceId(offer);
        validateCandidate(offer);        
        if(!offer.isResolved__c)
        validateMultiplePayRate(offer);
        upsertErr(offer);
    }
    public void validateMultiplePayRate(Offer__c offer)
    {
        List<Payment_Rates__c> payRateList = new List<Payment_Rates__c>();
        payRateList = getMatchingPayrates(offer);
        if(payRateList == NULL || payRateList.size() == 0)
        {
            faledValidationMsg.add('No Matching pay rate found');
        }
        else
        {
            if(payRateList.size() > 1)
                faledValidationMsg.add('Multiple matching pay rates record found');
            else
            {
                //offer.Payment_Rate__c = payRateList[0].id;
                //update offer;           
            }
        }
    }
    
    public void upsertErr(Offer__c offer)
    {
        Validation_Error__c tempError = new Validation_Error__c();
        for(String msg : faledValidationMsg)
        {
            if(oldErrorMap.get(msg) != NULL)
            {
                tempError = oldErrorMap.get(msg);
                if(tempError.Is_Corrected__c == True && tempError.Is_Overriden__c != True){
                    tempError.Is_Corrected__c = False;
                    errorToUpsert.add(tempError);
                }
            }
            else
            {
                errorToUpsert.add(new Validation_Error__c(Err_Msg__c = msg, Offer__c = offer.id));
            }
        }
        for(Validation_Error__c err :  oldErrorMap.Values())
        {            
            if(!faledValidationMsg.Contains(err.Err_Msg__c) && err.Is_Overriden__c != True)
            {
                err.Is_Corrected__c = True;
                    errorToUpsert.add(err);
            }
        }
        if(errorToUpsert.size()>0)
            upsert errorToUpsert;
    }
    
    public void validateAge(Offer__c offer)
    {
        if(offer.Candidate__r.Age__c < 15)
            faledValidationMsg.add('Age: Candidate younger than 15');
        else if(offer.Candidate__r.Age__c > 75)
            faledValidationMsg.add('Age: Candidate older than 75');
    }
    public void validateCandidate(Offer__c offer)
    {
        if(offer.Candidate__c == NULL )
            faledValidationMsg.add('Candidate is blank in Offer');
    }    
    public void validateGradeId(Offer__c offer)
    {
        if(offer.Payment_Rate__c == NULL)
            faledValidationMsg.add('Grade Id is blank in Offer');
    }
    public void validatePostId(Offer__c offer)
    {
        if(offer.Post_Code__c == NULL || offer.Post_Code__c == '')
            faledValidationMsg.add('Post Code is blank in Offer');
    }
    public void validateServiceId(Offer__c offer)
    {
        if(offer.Service_Code__c == NULL || offer.Service_Code__c == '')
            faledValidationMsg.add('Service Code is blank in Offer');
    }
    public List<Payment_Rates__c> getMatchingPayrates(Offer__c offer)
    {
        List<Payment_Rates__c> mathingPayRate = new List<Payment_Rates__c>();
        List<Payment_Rates__c> finalPayRate = new List<Payment_Rates__c>();
        mathingPayRate = [Select id, Min_Age__c, Max_Age__c, Job_Code__c, Post_Code__c, Service_Code__c, Zone_Id__c from Payment_Rates__c where Job_Code__c =: offer.Job_Code__c];
        if(mathingPayRate.size()>0)
        {
            if(mathingPayRate.size() > 1)
            {
                //It will search for the candidate age
                ManualInterventionResult = 'Multiple Pay Rate Found';
                finalPayRate = checkForZone(offer, mathingPayRate);
                system.debug('----finalPayRate' + finalPayRate);
                
                if(finalPayRate.size() > 0)
                    return finalPayRate;
                else
                {
                   return mathingPayRate;
                }
                
            }
            else
            {
                ManualInterventionResult = 'One Pay Rate Found';
                Return mathingPayRate;
            }
        }
        else
        {
            ManualInterventionResult = 'NO Pay Rate Found';
            Return finalPayRate;        
        }
    }
    
    public List<Payment_Rates__c> checkForZone(Offer__c offer, List<Payment_Rates__c> payRatesToProcess)
    {       
        List<Payment_Rates__c> processedPayRates = new List<Payment_Rates__c>();
        for(Payment_Rates__c pr : payRatesToProcess)
        {
            if(pr.Zone_Id__c == offer.Pay_Zone__c)
            {
                processedPayRates.add(pr);
            }
        }
        system.debug('----processedPayRates'+processedPayRates);
        return processedPayRates;
        /*
        if(processedPayRates.size()>0)
        {
            return processedPayRates;
        }
        else
        {
            return NULL;
        }
        */
    }
    public List<Payment_Rates__c> checkCandidateAge(Offer__c offer, List<Payment_Rates__c> payRatesToProcess)
    {
        List<Payment_Rates__c> processedPayRates = new List<Payment_Rates__c>();
        for(Payment_Rates__c pr : payRatesToProcess)
        {
            if(offer.Candidate__r.Age__c >= pr.Min_Age__c && offer.Candidate__r.Age__c <= pr.Min_Age__c)
            {
                processedPayRates.add(pr);
            }           
        }
        system.debug('----processedPayRates -- checkCandidateAge'+processedPayRates);
        return processedPayRates;
    }
}