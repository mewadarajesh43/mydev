public class CreateCSVRowsController
{
    public String csvType {get;set;}
    public List<String> fieldList {get{
    for(String s : CSVGenerationHelper.csvSettings.get(csvType).fieldAPINames)
    {
        if(s.contains('::')){
        fieldCombination = new Map<String,List<String>>();
            fieldCombination.put(s,s.split('::'));
            }
    }
    return CSVGenerationHelper.csvSettings.get(csvType).fieldAPINames;}set;}
    public List<String> headerList {get{return CSVGenerationHelper.csvSettings.get(csvType).headers;}set;}
    public Map<String,List<String>> fieldCombination{get;set;}
       
}