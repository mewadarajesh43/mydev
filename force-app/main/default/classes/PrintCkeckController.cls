Public class PrintCkeckController
{    
    public List<ThreeCheck_Wrapper> tcwList{get;set;}
    public Integer tcwListSize {get;set;}
    private Integer startIndex ;
    public PrintCkeckController()    
    {        
        tcwList = new List<ThreeCheck_Wrapper>();
        tcwListSize = 0;
        
        startIndex = 3;
        SingleCheck_Wrapper scw;
        List<SingleCheck_Wrapper> scwList = new List<SingleCheck_Wrapper>();
         /*
        for(Integer i=1;i<startIndex ;i++)
        {
            scw = new SingleCheck_Wrapper(Null,Null,Null,Null);
            scwList.add(scw);
        }
       
        for(Payment__c payment : [Select id, Amount__c, Amount_In_Words__c, Payment_Date__c, Pay_To__c from Payment__c])
        {
            paymentList.add(payment);
        }
        */
       
        ThreeCheck_Wrapper tcw;
       
        
        scwList.add(new SingleCheck_Wrapper(5000,'Test 1','Five Thousand',System.Today()));
        scwList.add(new SingleCheck_Wrapper(6000,'Test 2','Six Thousand',System.Today()));
        scwList.add(new SingleCheck_Wrapper(7000,'Test 3','Seven Thousand',System.Today()));
        
        for(Integer i=0;i<3;i++) {         
            
           // scw = new SingleCheck_Wrapper(paymentlist[i].Amount__c,
            //                                paymentlist[i].Pay_To__c ,paymentlist[i].Amount_In_Words__c,paymentlist[i].Payment_Date__c);
           // scwList.add(scw);
          // scwList.add(new SingleCheck_Wrapper(5000,'Test 1','Five Thousand',System.Today()));
            
            Integer pageNo = 0;           
           // if(i!= 0 && (math.mod(i,3)==2 || i+1== paymentlist.size()))
            if(i!= 0 && (math.mod(i,3)==2 || i+1== 3))
            {            
                pageNo = pageNo+1;
                tcw = new ThreeCheck_Wrapper(scwList,pageNo);
                tcwList.add(tcw);
                system.debug('gggg'+scwList.size());
                scwList.clear();
                
            }
            
        }
       
        tcwListSize = scwList.size();
       
    }
    
    class ThreeCheck_Wrapper
    {
        public List<SingleCheck_Wrapper> SCWrapperList{get;set;}
        public Integer pageNumber {get;set;}
        public ThreeCheck_Wrapper(List<SingleCheck_Wrapper> SCWrapperList, Integer pageNumber)
        {
           this.SCWrapperList = new List<SingleCheck_Wrapper>();
           this.pageNumber = pageNumber;
           for(SingleCheck_Wrapper scw : SCWrapperList)
           this.SCWrapperList.add(scw);
        }
    }
    
    class SingleCheck_Wrapper
    {
        public Decimal amount {get;set;}
        public String payTo {get;set;}
        public String amountInWords {get;set;}
        public  Date paymentDate {get;set;}
        public singleCheck_Wrapper(Decimal amount, String payTo, String amountInWords, Date paymentDate)
        {
            this.amount= amount;
            this.payTo = payTo;
            this.amountInWords = amountInWords;
            this.paymentDate = paymentDate ;
        }
    }
}