global class ActivityChaseController implements Database.Batchable<sObject>, Database.Stateful{

   global date today = System.today();
   Global String status = 'Completed';
   global final String Query = 'Select id,Name, Chase_Done__c, Next_Chase_Date__c, Status__c, Attention_Required__c from Activity__c where Next_Chase_Date__c =: today AND status__c != : status';
   global integer Summary;
  
   global ActivityChaseController(){
       
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
   
   global void execute(
                Database.BatchableContext BC, 
                List<sObject> scope){   
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {'mewadarajesh43@gmail.com', 'emrdavis@gmail.com'});
      mail.setSubject('Chase Email');
      String htmlBody = 'Dear Rajesh Mewada <br></br> <br></br> You have following activity to complete for the offer to be processed further. <br></br>';
      Integer count = 1;
      Integer chaseDelay = 0;
     
      
      List<Holiday> holidayList = new list<Holiday>();
      holidayList = [select id, name, ActivityDate from holiday];
      set<Date> holidaySet = new Set<Date>();
      
      for(Activity__c s : (List<Activity__c>)scope){
        
        htmlBody = htmlBody + (String.valueOf(count++)) + '. '+ s.Name + ' Completion <br></br>';
        chaseDelay = (s.Chase_Done__c != Null) ? (Integer)(s.Chase_Done__c +1) : 1;
        s.Chase_Done__c = chaseDelay;
        s.Attention_Required__c = 'Chase Delay' + String.valueOf(chaseDelay);
        
        Date nextChaseDate;
        Date tempDate = s.Next_Chase_Date__c + 1;
        for(Holiday h : holidayList)
        {
            holidaySet.add(h.ActivityDate);
        }
        while(nextChaseDate == NULL)
        {
            if(holidaySet.contains(tempDate+1) )
            {
                tempDate = tempDate+1;
            }
            else
            {
                nextChaseDate = tempDate+1;
            }
        }
        s.Next_Chase_Date__c = nextChaseDate;
     } 
      htmlBody = htmlBody + '<br></br> Thanks & Regards <br></br> Boots Team';
      mail.setHtmlBody(htmlBody);
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      update scope;
   }
   

global void finish(Database.BatchableContext BC){
 }
}