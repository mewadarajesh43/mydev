public class CSVGenerationHelper
{
    
                  
  public final static Map<String, csvSettingWrapper> csvSettings     = new Map<String, csvSettingWrapper>{
     
        'ASN Update'  => new csvSettingWrapper(
                         new list<String>{'ASN Number','Booking Reference','Customer ID','Customer Name','Product Name','Product Id','Quantity','Contact Name','Contact Email','Contact Fax','Contact Telephone','Scheduled Delivery Date','Scheduled Delivery Time','Delivery Location'}, 
                         new list<String>{'ASN_Consignment__r.ASN_Number__c', 'ASN_Consignment__r.Order_Reference__c', 'ASN_Consignment__r.CustomerId__c','ASN_Consignment__r.CustomerName__c','Product_Name__c','Product_Id__c','Product_Quantity__c','ASN_Consignment__r.ContactName__c','ASN_Consignment__r.ContactEmail__c','ASN_Consignment__r.ContactFax__c','ASN_Consignment__r.ContactTelephone__c','ASN_Consignment__r.ScheduledDeliveryDate__c','ASN_Consignment__r.ScheduledDeliveryTime__c', 'ASN_Consignment__r.DeliveryAddressCity__c::ASN_Consignment__r.DeliveryAddressLine1__c::ASN_Consignment__r.DeliveryPostCode__c'})
                      ,
        'ASN Update Part'  => new csvSettingWrapper(
                              new list<String>{'Product Name','Product Code','ASNMainReference','Identifier Type','Identifier',' Manufacturer Country','Manufacturer Name','Pallet Identifier','Carton Identifier'}, 
                              new list<String>{'ASN_Product_Details__r.ProductName__c','ASN_Product_Details__r.ProductId__c','ASN_Product_Details__r.ASN_Container_Details__r.ASN_Details__r.ASNMainReference__c','IdentifierType__c','Identifier__c','ASN_Product_Details__r.CountryOfManufacture__c','ASN_Product_Details__r.ManufacturerName__c','ASN_Product_Details__r.ASN_Container_Details__r.ASN_Container_Details__r.ContainerId__c','ASN_Product_Details__r.ASN_Container_Details__r.ContainerId__c'})
                            };
    
   
   
    class csvSettingWrapper
    {
        public List<String> headers;
        public List<String> fieldAPINames;
        public csvSettingWrapper(List<String> headers,List<String> fieldAPINames)
        {
            this.headers = headers;
            this.fieldAPINames = fieldAPINames;
        }
        
    }
    
}